/**
 * @file Root reducers
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import IStore from './store/IStore';

// Place for reducers' app
import AuthPage, { name as nameOfAuthPage } from '../modules/Auth';
import LessonPage, { name as nameOfLessonPage } from '../modules/Lesson';
import LearningPage, { name as nameOfLearningPage } from '../modules/Learning';
import VocabularyPage, { name as nameOfVocabularyPage } from '../modules/Vocabulary';
import ProfilePage, { name as nameOfProfilePage } from '../modules/Profile';
import MainLayout, { name as nameOfMainLayout } from '../layouts/MainLayout';

/*----Reducers List-----------------*/
const reducers = {
	[nameOfAuthPage]: AuthPage,
	[nameOfVocabularyPage]: VocabularyPage,
	[nameOfLearningPage]: LearningPage,
	[nameOfLessonPage]: LessonPage,
	[nameOfProfilePage]: ProfilePage,
	[nameOfMainLayout]: MainLayout,
};

const rootReducer = (history: History) =>
	combineReducers<IStore>({
		...reducers,
		router: connectRouter(history),
	});

export default rootReducer;
