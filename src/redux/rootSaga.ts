/**
 * @file root sagas
 */

import { all } from 'redux-saga/effects';

// Place for sagas' app
import { sagas as AuthSaga } from '../modules/Auth';
import { sagas as VocabularySaga } from '../modules/Vocabulary';
import { sagas as LessonSaga } from '../modules/Lesson';
import { sagas as ProfileSaga } from '../modules/Profile';
import { sagas as LearningSaga } from '../modules/Learning';
import { sagas as MainLayout } from '../layouts/MainLayout';

/*----Sagas List-----------------*/
export default function* rootSaga() {
	yield all([
		AuthSaga(),
		LessonSaga(),
		ProfileSaga(),
		LearningSaga(),
		VocabularySaga(),
		MainLayout(),
	]);
}
