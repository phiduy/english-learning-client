import { initialState as AuthInitialState, name as AuthPage } from '../../modules/Auth';
import { initialState as LessonInitialState, name as LessonPage } from '../../modules/Lesson';
import { initialState as LearningInitialState, name as LearningPage } from '../../modules/Learning';
import { initialState as ProfileInitialState, name as ProfilePage } from '../../modules/Profile';
import {
	initialState as VocabularyInitialState,
	name as VocabularyPage,
} from '../../modules/Vocabulary';
import {
	initialState as MainLayoutInitialState,
	name as MainLayout,
} from '../../layouts/MainLayout';
import IStore from './IStore';

export const initialState: IStore = {
	[VocabularyPage]: VocabularyInitialState,
	[LearningPage]: LearningInitialState,
	[ProfilePage]: ProfileInitialState,
	[LessonPage]: LessonInitialState,
	[AuthPage]: AuthInitialState,
	[MainLayout]: MainLayoutInitialState,
	router: null,
};

export default initialState;
