import { IVocabularyState, name as VocabularyPageState } from '../../modules/Vocabulary';
import { ILessonState, name as LessonPageState } from '../../modules/Lesson';
import { ILearningState, name as LearningPageState } from '../../modules/Learning';
import { IAuthState, name as AuthPageState } from '../../modules/Auth';
import { IProfileState, name as ProfilePageState } from '../../modules/Profile';
import { IMainLayoutState, name as MainLayoutState } from '../../layouts/MainLayout';

export default interface IStore {
	[AuthPageState]: IAuthState;
	[ProfilePageState]: IProfileState;
	[LessonPageState]: ILessonState;
	[LearningPageState]: ILearningState;
	[VocabularyPageState]: IVocabularyState;
	[MainLayoutState]: IMainLayoutState;
	router: any;
}
