import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';

export const Avatar: React.FC<{ url: string }> = ({ url }) => {
	return (
		<div className="avatar">
			<Link to="/profile">
				<img
					// src="http://duolingo-images.s3.amazonaws.com/avatars/614873523/1FjwcF6L55/medium"
					src={
						url
							? `${process.env.REACT_APP_BASE_URL}${url}`
							: 'http://duolingo-images.s3.amazonaws.com/avatars/614873523/1FjwcF6L55/medium'
					}
					alt="user_avatar"
				/>
			</Link>
		</div>
	);
};
