export const routeName = {
	auth: '/auth',
	resetPassword: '/reset-password',
	shop: '/shop',
	vocabulary: '/vocabulary',
	profile: '/profile',
	learn: '/learn',
	lesson: '/lesson/:lessonId',
};
