import { takeEvery, call, put, delay } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as UserApi from '../../../api/user';

export function* handleGetUserInfo(action: any) {
	try {
		const res = yield call(UserApi.getUserInfo);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.getUserInfoSuccess(res.data.data));
		} else {
			console.log('data', res.data);
			const { errors } = res.data;
			message.error(errors[0].message, 3);
			yield put(actions.getUserInfoFail(res.data));
		}
	} catch (error) {
		yield put(actions.getUserInfoFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetUserInfo() {
	yield takeEvery(Keys.GET_USER_INFO, handleGetUserInfo);
}
/*-----------------------------------------------------------------*/
const sagas = [watchGetUserInfo];
export default sagas;
