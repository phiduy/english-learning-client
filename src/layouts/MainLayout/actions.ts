import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IUserInfo, MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export const toggleModal = (data: { type: MAIN_LAYOUT_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: {
			type: data.type,
		},
	};
};

//#region GET User Info Actions
export const getUserInfo = (): IActions.IGetUserInfo => {
	return {
		type: Keys.GET_USER_INFO,
	};
};

export const getUserInfoSuccess = (data: IUserInfo): IActions.IGetUserInfoSuccess => {
	return {
		type: Keys.GET_USER_INFO_SUCCESS,
		payload: data,
	};
};

export const getUserInfoFail = (res: any): IActions.IGetUserInfoFail => {
	return {
		type: Keys.GET_USER_INFO_FAIL,
		payload: res,
	};
};
//#endregion
