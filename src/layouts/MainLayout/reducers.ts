import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IMainLayoutState, initialState, MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export const name = 'MainLayout';

export const reducer: Reducer<IMainLayoutState, any> = (
	state: IMainLayoutState = initialState,
	action: ActionTypes
): IMainLayoutState => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.GET_USER_INFO:
			return onGetUserInfo(state, action);
		case Keys.GET_USER_INFO_SUCCESS:
			return onGetUserInfoSuccess(state, action);
		case Keys.GET_USER_INFO_FAIL:
			return onGetUserInfoFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: IMainLayoutState, action: IActions.IToggleModal) => {
	const { type } = action.payload;

	switch (type) {
		case MAIN_LAYOUT_MODAL.CHANGE_USER_PASSWORD:
			return {
				...state,
				toggleModalChangeUserPassword: !state.toggleModalChangeUserPassword,
			};
		default:
			return {
				...state,
			};
	}
};

const onGetUserInfo = (state: IMainLayoutState, action: IActions.IGetUserInfo) => {
	return {
		...state,
		isLoadingUserInfo: true,
	};
};
const onGetUserInfoSuccess = (state: IMainLayoutState, action: IActions.IGetUserInfoSuccess) => {
	return {
		...state,
		isLoadingUserInfo: false,
		userInfo: action.payload,
	};
};
const onGetUserInfoFail = (state: IMainLayoutState, action: IActions.IGetUserInfoFail) => {
	return {
		...state,
		isLoadingUserInfo: false,
	};
};
