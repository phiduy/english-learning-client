export enum MAIN_LAYOUT_MODAL {
	CHANGE_USER_PASSWORD = 1,
}

export interface IUserInfo {
	email: string;
	gender: string;
	name: string;
	dayOfBirth: string;
	avatar?: string;
	gamesPass: {
		game: string;
		passAt: string;
		_id: string;
	}[];
	levelInfo: { level: number; order: number };
	_id: string;
}

export interface IMainLayoutState {
	toggleModalChangeUserPassword: boolean;
	isLoadingUserInfo: boolean;
	userInfo?: IUserInfo;
}

// InitialState
export const initialState: IMainLayoutState = {
	toggleModalChangeUserPassword: false,
	isLoadingUserInfo: false,
	userInfo: undefined,
};
