/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import { IUserInfo, MAIN_LAYOUT_MODAL } from './model/IMainLayoutState';

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: MAIN_LAYOUT_MODAL;
	};
}

export interface IGetUserInfo extends Action {
	readonly type: Keys.GET_USER_INFO;
}

export interface IGetUserInfoSuccess extends Action {
	readonly type: Keys.GET_USER_INFO_SUCCESS;
	payload: IUserInfo;
}

export interface IGetUserInfoFail extends Action {
	readonly type: Keys.GET_USER_INFO_FAIL;
	payload?: {
		errors: IError[];
	};
}
