/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IToggleModal
	| IActions.IGetUserInfo
	| IActions.IGetUserInfoSuccess
	| IActions.IGetUserInfoFail;

export default ActionTypes;
