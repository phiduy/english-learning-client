import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IVocabularyProps {
	store: IStore;
	actions: typeof Actions;
}
