import { IPagination } from '../../../common/interfaces';

export enum VOCABULARY_CLEAR {
	CREATE_USER_HISTORY = 1,
}

export interface IVocabulary {
	audio: string;
	english: string;
	example: string;
	exampleTranslate: string;
	explain: string;
	image: string;
	spell: string;
	vietnamese: string;
	_id: string;
}

export interface IVocabularyPackage {
	isLeaf: boolean;
	name: string;
	ancestors: string[];
	packageImage: string;
	_id: string;
	vocabularies: { _id: string; vocabulary: IVocabulary }[];
}

export interface IVocabularyState {
	isProcessing: boolean;
	isUserHistoryCreateSuccess: boolean;
	isLoadingVocabularyPackage: boolean;
	currentVocabularyPackage?: IVocabularyPackage;
	vocabularyPackages: { data: IVocabularyPackage[]; pagination: IPagination };
}

// InitialState
export const initialState: IVocabularyState = {
	isProcessing: false,
	isLoadingVocabularyPackage: false,
	isUserHistoryCreateSuccess: false,
	currentVocabularyPackage: undefined,
	vocabularyPackages: {
		data: [],
		pagination: {
			totalRecord: 0,
			totalPage: 0,
		},
	},
};
