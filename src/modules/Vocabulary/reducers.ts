import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IVocabularyState, initialState } from './model/IVocabularyState';

export const name = 'VocabularyPage';

export const reducer: Reducer<IVocabularyState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.HANDLE_CURRENT_VOCABULARY_PACKAGE:
			return onHandleCurrentVocabularyPackage(state, action);

		case Keys.GET_VOCABULARY:
			return onGetVocabulary(state, action);
		case Keys.GET_VOCABULARY_SUCCESS:
			return onGetVocabularySuccess(state, action);
		case Keys.GET_VOCABULARY_FAIL:
			return onGetVocabularyFail(state, action);

		case Keys.POST_USER_HISTORY:
			return onPostUserHistory(state, action);
		case Keys.POST_USER_HISTORY_SUCCESS:
			return onPostUserHistorySuccess(state, action);
		case Keys.POST_USER_HISTORY_FAIL:
			return onPostUserHistoryFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action
const onHandleClear = (state: IVocabularyState, action: IActions.IHandleClear) => {
	return {
		...state,
		isUserHistoryCreateSuccess: false,
	};
};

const onHandleCurrentVocabularyPackage = (
	state: IVocabularyState,
	action: IActions.IHandleCurrentVocabularyPackage
) => {
	return {
		...state,
		currentVocabularyPackage: action.payload,
	};
};

const onGetVocabulary = (state: IVocabularyState, action: IActions.IGetVocabulary) => {
	return {
		...state,
		isLoadingVocabularyPackage: true,
	};
};
const onGetVocabularySuccess = (
	state: IVocabularyState,
	action: IActions.IGetVocabularySuccess
) => {
	return {
		...state,
		isLoadingVocabularyPackage: false,
		vocabularyPackages: action.payload,
	};
};
const onGetVocabularyFail = (state: IVocabularyState, action: IActions.IGetVocabularyFail) => {
	return {
		...state,
		isLoadingVocabularyPackage: false,
	};
};

const onPostUserHistory = (state: IVocabularyState, action: IActions.IPostUserHistory) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onPostUserHistorySuccess = (
	state: IVocabularyState,
	action: IActions.IPostUserHistorySuccess
) => {
	return {
		...state,
		isProcessing: false,
		isUserHistoryCreateSuccess: true,
	};
};
const onPostUserHistoryFail = (state: IVocabularyState, action: IActions.IPostUserHistoryFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
