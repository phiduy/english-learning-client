/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IHandleCurrentVocabularyPackage
	| IActions.IGetVocabulary
	| IActions.IGetVocabularySuccess
	| IActions.IGetVocabularyFail
	| IActions.IPostUserHistory
	| IActions.IPostUserHistorySuccess
	| IActions.IPostUserHistoryFail;

export default ActionTypes;
