import * as React from 'react';
import { RouteComponentProps, useParams } from 'react-router-dom';
import { IVocabularyProps } from '../../model/IVocabularyProps';
import { CloseOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import { VocabularyChildPackage } from './VocabularyChildPackage';
import '../index.scss';

interface IProps extends RouteComponentProps, IVocabularyProps {}

export const VocabularyDetail: React.FC<IProps> = (props) => {
	const { vocabularyPackages, isLoadingVocabularyPackage } = props.store.VocabularyPage;
	const param: {
		vocabularyPackageId: string;
	} = useParams();

	React.useEffect(() => {
		props.actions.getVocabulary({
			parent: param.vocabularyPackageId,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<div className="lesson_container vocabulary_wrapper">
				<div style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0 }}>
					<div className="challenge_container">
						<div className="challenge_process_container">
							<div className="challenge_process">
								<div className="process_wrapper">
									<div className="doan_btn_quit" onClick={() => props.history.push('/vocabulary')}>
										<CloseOutlined />
									</div>
									<div className="process_bar">
										<div className="process_bar_inner" />
									</div>
								</div>
							</div>
						</div>
						<div className="vocabulary_inner">
							<div className="vocabulary_package_list">
								<h1>Danh sách bài từ vựng</h1>
								<Spin spinning={isLoadingVocabularyPackage}>
									{vocabularyPackages.data.map((item) => (
										<VocabularyChildPackage key={item._id} item={item} props={props} />
									))}
								</Spin>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
