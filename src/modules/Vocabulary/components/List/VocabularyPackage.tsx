import React from 'react';
import { IVocabularyProps } from '../../model/IVocabularyProps';
import { RouteComponentProps } from 'react-router-dom';
import { IVocabularyPackage } from '../../model/IVocabularyState';

interface IProps extends IVocabularyProps, RouteComponentProps {}

interface IVocabularyPackageProps {
	props: IProps;
	item: IVocabularyPackage;
}

export const VocabularyPackage: React.FC<IVocabularyPackageProps> = ({ props, item }) => {
	return (
		<label
			className="vocabulary_package"
			onClick={() => {
				props.history.push(`/vocabulary/${item._id}`);
			}}
		>
			<img
				src={
					item.packageImage
						? item.packageImage
						: 'http://d35aaqx5ub95lt.cloudfront.net/images/user-motivation-survey/family-and-friends.svg'
				}
				alt={item.name}
			/>
			<div className="title">{item.name}</div>
		</label>
	);
};
