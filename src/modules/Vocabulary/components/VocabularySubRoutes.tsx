import React from 'react';
// import { Transition } from "react-transition-group";
import { Switch, Route, Redirect, useRouteMatch, RouteComponentProps } from 'react-router-dom';
import { IVocabularyProps } from '../model/IVocabularyProps';
import { VocabularyDetail } from './Detail';
import { VocabularyLearn } from './Learn';
import { VocabularyList } from './List';

interface IProps extends IVocabularyProps, RouteComponentProps {}

export const VocabularySubRoutes: React.FC<IProps> = (props) => {
	const { url } = useRouteMatch();
	return (
		<Switch>
			<Route
				path={`${url}/list`}
				exact={true}
				children={() => <VocabularyList {...props} />}
			/>
			<Route
				path={`${url}/:vocabularyPackageId`}
				exact={true}
				children={() => <VocabularyDetail {...props} />}
			/>
			<Route
				path={`${url}/:vocabularyPackageId/learn`}
				exact={true}
				children={() => <VocabularyLearn {...props} />}
			/>
			<Redirect from="*" to={`${url}/list`} />
		</Switch>
	);
};
