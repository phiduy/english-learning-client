import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IVocabularyProps } from '../model/IVocabularyProps';
import { VocabularySubRoutes } from './VocabularySubRoutes';
import './index.scss';

interface IProps extends RouteComponentProps, IVocabularyProps {}

export const VocabularyPage: React.FC<IProps> = (props) => {
	return (
		<React.Fragment>
			<VocabularySubRoutes {...props} />
		</React.Fragment>
	);
};
