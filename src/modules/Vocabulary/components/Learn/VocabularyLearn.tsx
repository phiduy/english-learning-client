import * as React from 'react';
import { RouteComponentProps, useHistory, useParams } from 'react-router-dom';
import { IVocabularyProps } from '../../model/IVocabularyProps';
import { Vocabulary } from './Vocabulary';
import { ProcessBar } from '../../../../components';
import { soundEffect } from '../../../../common/constants';
import { VocabularyResult } from './Result';
import { VOCABULARY_CLEAR } from '../../model/IVocabularyState';

interface IProps extends RouteComponentProps, IVocabularyProps {}

export const VocabularyLearn: React.FC<IProps> = (props) => {
	const {
		currentVocabularyPackage,
		isUserHistoryCreateSuccess,
		vocabularyPackages,
	} = props.store.VocabularyPage;
	const [vocabularyIndex, setVocabularyIndex] = React.useState(0);
	const vocabularyLength = currentVocabularyPackage?.vocabularies.length as number;
	const [toggleResult, setToggleResult] = React.useState<boolean>(false);

	const history = useHistory();

	const param: {
		vocabularyPackageId: string;
	} = useParams();

	React.useEffect(() => {
		if (currentVocabularyPackage === undefined) {
			const index = vocabularyPackages.data.findIndex(
				(item) => item._id === param.vocabularyPackageId
			);
			if (index > -1) {
				props.actions.handleCurrentVocabularyPackage(vocabularyPackages.data[index]);
			} else {
				history.push('/vocabulary');
			}
		}
		if (isUserHistoryCreateSuccess) {
			history.push('/vocabulary');
		}
		return () => {
			props.actions.handleClear({
				type: VOCABULARY_CLEAR.CREATE_USER_HISTORY,
			});
		};
		// eslint-disable-next-line
	}, [isUserHistoryCreateSuccess]);

	const onNextVocabulary = () => {
		if (toggleResult) {
			props.history.push(`/vocabulary/${param.vocabularyPackageId}`);
		} else {
			const nextQuestionIndex = (vocabularyIndex + 1) as number;
			if (nextQuestionIndex < vocabularyLength - 1) {
				setVocabularyIndex(nextQuestionIndex);
				const audio = new Audio(soundEffect.right);
				audio.play();
			} else {
				const audio = new Audio(soundEffect.steak);
				audio.play();
				setToggleResult(true);
			}
		}
	};

	const onBackVocabulary = () => {
		const backQuestionIndex = (vocabularyIndex - 1) as number;
		if (backQuestionIndex >= 0) {
			setVocabularyIndex(backQuestionIndex);
		}
	};

	return (
		<React.Fragment>
			<div className="lesson_container">
				<div style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0 }}>
					<div className="challenge_container">
						{toggleResult ? (
							<VocabularyResult {...props} />
						) : (
							<React.Fragment>
								<ProcessBar
									onAlertOk={() => {
										history.push('/vocabulary');
									}}
									questionLength={vocabularyLength}
									questionIndex={vocabularyIndex}
								/>
								<div className="challenge_content_container">
									{currentVocabularyPackage && (
										<div className="challenge_content justify-content-center">
											<div className="challenge_header">
												<h1>{currentVocabularyPackage?.name}</h1>
											</div>
											<Vocabulary item={currentVocabularyPackage?.vocabularies[vocabularyIndex]} />
										</div>
									)}
								</div>
							</React.Fragment>
						)}

						<div className="challenge_footer_container">
							<div className="challenge_footer">
								{!toggleResult && (
									<div className="cancel_btn" onClick={onBackVocabulary}>
										<button className="doan_btn">Quay lại</button>
									</div>
								)}
								<div className="next_btn">
									<button className="doan_btn  doan_btn_success" onClick={onNextVocabulary}>
										Tiếp tục
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
