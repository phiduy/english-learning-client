import React from 'react';
import { IVocabulary } from '../../model/IVocabularyState';

interface IVocabularyProps {
	item?: { _id: string; vocabulary: IVocabulary };
}

export const Vocabulary: React.FC<IVocabularyProps> = ({ item }) => {
	const vocabulary = item?.vocabulary as IVocabulary;

	return (
		<div className="vocabulary">
			<div className="flip-card">
				<div className="flip-card-inner">
					<div className="flip-card-front">
						<div className="example_image">
							<img src={vocabulary.image} alt="Avatar" />
						</div>
						<h2 className="mb-0">{vocabulary.english}</h2>
						<span>{vocabulary.spell}</span>
					</div>
					<div className="flip-card-back">
						<p>{vocabulary.vietnamese}</p>
					</div>
				</div>
			</div>
			<div className="vocabulary_desc">
				{/* <div className="pronoun">
					<span className="font-weight-bold mr-2">{vocabulary.english}</span>
					<span className="text-primary">{vocabulary.spell}</span>
				</div> */}
				<div className="explain">
					<span className="title">Giải thích:</span>
					<span>{vocabulary.explain}</span>
				</div>
				{/* <div className="word_type">
					<span className="title">Loại từ:</span>
					<span>{vocabulary.vietnamese}</span>
				</div> */}
				<div className="example">
					<div className="title">Ví dụ:</div>
					<div>{vocabulary.example}</div>
					<div className="translate_example font-weight-bold">{vocabulary.example}</div>
				</div>
			</div>
		</div>
	);
};
