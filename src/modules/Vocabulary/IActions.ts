/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { IError, IPagination } from '../../common/interfaces';
import { IVocabularyPackage, VOCABULARY_CLEAR } from './model/IVocabularyState';

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: VOCABULARY_CLEAR;
	};
}

export interface IHandleCurrentVocabularyPackage extends Action {
	readonly type: Keys.HANDLE_CURRENT_VOCABULARY_PACKAGE;
	payload: IVocabularyPackage;
}

//#region User Login IActions
export interface IGetVocabulary extends Action {
	readonly type: Keys.GET_VOCABULARY;
	payload: {
		root?: boolean;
		parent?: string;
		onlyChild?: boolean;
		page?: number;
		limit?: number;
	};
}

export interface IGetVocabularySuccess extends Action {
	readonly type: Keys.GET_VOCABULARY_SUCCESS;
	payload: { data: IVocabularyPackage[]; pagination: IPagination };
}

export interface IGetVocabularyFail extends Action {
	readonly type: Keys.GET_VOCABULARY_FAIL;
	payload?: {
		message: string;
	};
}
//#endregion

//#region User History IActions
export interface IPostUserHistory extends Action {
	readonly type: Keys.POST_USER_HISTORY;
	payload: {
		isAddVocabularyLevel: boolean;
	};
}

export interface IPostUserHistorySuccess extends Action {
	readonly type: Keys.POST_USER_HISTORY_SUCCESS;
}

export interface IPostUserHistoryFail extends Action {
	readonly type: Keys.POST_USER_HISTORY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
