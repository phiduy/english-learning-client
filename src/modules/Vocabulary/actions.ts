import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IVocabularyPackage, VOCABULARY_CLEAR } from './model/IVocabularyState';
import { IError, IPagination } from '../../common/interfaces';

export const handleClear = (data: { type: VOCABULARY_CLEAR }): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: data,
	};
};

export const handleCurrentVocabularyPackage = (
	data: IVocabularyPackage
): IActions.IHandleCurrentVocabularyPackage => {
	return {
		type: Keys.HANDLE_CURRENT_VOCABULARY_PACKAGE,
		payload: data,
	};
};

//#region GET Vocabulary Actions
export const getVocabulary = (data: {
	root?: boolean;
	parent?: string;
	onlyChild?: boolean;
	page?: number;
	limit?: number;
}): IActions.IGetVocabulary => {
	return {
		type: Keys.GET_VOCABULARY,
		payload: data,
	};
};

export const getVocabularySuccess = (res: {
	data: IVocabularyPackage[];
	pagination: IPagination;
}): IActions.IGetVocabularySuccess => {
	return {
		type: Keys.GET_VOCABULARY_SUCCESS,
		payload: res,
	};
};

export const getVocabularyFail = (res: any): IActions.IGetVocabularyFail => {
	return {
		type: Keys.GET_VOCABULARY_FAIL,
		payload: res,
	};
};
//#endregion

//#region POST User History Actions
export const postUserHistory = (data: {
	isAddVocabularyLevel: boolean;
}): IActions.IPostUserHistory => {
	return {
		type: Keys.POST_USER_HISTORY,
		payload: data,
	};
};

export const postUserHistorySuccess = (res: any): IActions.IPostUserHistorySuccess => {
	return {
		type: Keys.POST_USER_HISTORY_SUCCESS,
	};
};

export const postUserHistoryFail = (res: IError[]): IActions.IPostUserHistoryFail => {
	return {
		type: Keys.POST_USER_HISTORY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
