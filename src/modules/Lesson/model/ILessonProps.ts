import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ILessonProps {
	store: IStore;
	actions: typeof Actions;
}
