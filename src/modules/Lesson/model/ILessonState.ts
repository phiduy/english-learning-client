export enum LESSON_MODAL {
	REVIEW_LESSON = 1,
}

export enum LESSON_CLEAR {
	ALL = 0,
}
export interface IUserLoginInfo {
	email: string;
	password: string;
}

export type AnswerStatus = 'right' | 'wrong' | 'notSubmit' | 'result';
export interface IAnswer {
	answer: string;
	audio?: string;
	image?: string;
	_id: string;
}
export interface IQuestion {
	_id: string;
	question: string;
	rightAnswer: string;
	type: 'pictures' | 'listening' | 'sentence';
	answers: IAnswer[];
	link: string;
}

export interface ILesson {
	name: string;
	_id: string;
	level: number;
	questions: IQuestion[];
	requireOrder: number;
	testId: string;
	icon: string;
	isLearn: boolean;
	order: number;
}

export interface IUserAnswer {
	question: string;
	userAnswer: string;
	rightAnswer: string;
	right: boolean;
}

export interface ILessonState {
	isProcessing: boolean;
	isCreatingUserHistoryLearn: boolean;
	isLoadingLessonDetail: boolean;
	isUserHistoryCreateSuccess: boolean;

	toggleModalLessonReview: boolean;
	lessonDetail?: ILesson;
	userAnswers?: IUserAnswer[];
}

// InitialState
export const initialState: ILessonState = {
	isProcessing: false,
	isLoadingLessonDetail: false,
	isCreatingUserHistoryLearn: false,
	isUserHistoryCreateSuccess: false,
	toggleModalLessonReview: false,
	lessonDetail: undefined,
	userAnswers: [],
};
