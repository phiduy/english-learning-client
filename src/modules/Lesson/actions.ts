import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { ILesson, LESSON_MODAL, IUserAnswer, LESSON_CLEAR } from './model/ILessonState';

export const handleClear = (data: { type: LESSON_CLEAR }): IActions.IHandleClear => {
	return {
		type: Keys.HANDLE_CLEAR,
		payload: data,
	};
};

export const handleUserAnswer = (data: IUserAnswer): IActions.IHandleUserAnswer => {
	return {
		type: Keys.HANDLE_USER_ANSWER,
		payload: data,
	};
};

export const toggleModal = (data: { type: LESSON_MODAL }): IActions.IToggleModal => {
	return {
		type: Keys.TOGGLE_MODAL,
		payload: data,
	};
};

//#region Get Lesson By Id Actions
export const getLessonById = (data: { _id: string }): IActions.IGetLessonById => {
	return {
		type: Keys.GET_LESSON_BY_ID,
		payload: data,
	};
};

export const getLessonByIdSuccess = (res: ILesson): IActions.IGetLessonByIdSuccess => {
	return {
		type: Keys.GET_LESSON_BY_ID_SUCCESS,
		payload: res,
	};
};

export const getLessonByIdFail = (res: IError[]): IActions.IGetLessonByIdFail => {
	return {
		type: Keys.GET_LESSON_BY_ID_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region POST User History Actions
export const postUserHistory = (data: {
	isAddGamesLevel: boolean;
	isAddVocabularyLevel: boolean;
	gameId: string;
}): IActions.IPostUserHistory => {
	return {
		type: Keys.POST_USER_HISTORY,
		payload: data,
	};
};

export const postUserHistorySuccess = (res: ILesson): IActions.IPostUserHistorySuccess => {
	return {
		type: Keys.POST_USER_HISTORY_SUCCESS,
		payload: res,
	};
};

export const postUserHistoryFail = (res: IError[]): IActions.IPostUserHistoryFail => {
	return {
		type: Keys.POST_USER_HISTORY_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Check Point Test Actions
export const checkPointTest = (data: { level: string }): IActions.ICheckPointTest => {
	return {
		type: Keys.CHECK_POINT_TEST,
		payload: data,
	};
};

export const checkPointTestSuccess = (res: ILesson): IActions.ICheckPointTestSuccess => {
	return {
		type: Keys.CHECK_POINT_TEST_SUCCESS,
		payload: res,
	};
};

export const checkPointTestFail = (res: { message: string }): IActions.ICheckPointTestFail => {
	return {
		type: Keys.CHECK_POINT_TEST_FAIL,
	};
};
//#endregion

//#region Finish Check Point Test Actions
export const finishCheckPointTest = (data: { _id: string }): IActions.IFinishCheckPointTest => {
	return {
		type: Keys.FINISH_CHECK_POINT_TEST,
		payload: data,
	};
};

export const finishCheckPointTestSuccess = (
	res: ILesson
): IActions.IFinishCheckPointTestSuccess => {
	return {
		type: Keys.FINISH_CHECK_POINT_TEST_SUCCESS,
		payload: res,
	};
};

export const finishCheckPointTestFail = (res: {
	message: string;
}): IActions.IFinishCheckPointTestFail => {
	return {
		type: Keys.FINISH_CHECK_POINT_TEST_FAIL,
	};
};
//#endregion
