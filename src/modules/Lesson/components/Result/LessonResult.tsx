import * as React from 'react';
import { ILessonProps } from '../../model/ILessonProps';
import { Player } from '@lottiefiles/react-lottie-player';

interface IProps extends ILessonProps {}

export const LessonResult: React.FC<IProps> = (props) => {
	// const lottieRef = React.useRef<undefined>(undefined);

	return (
		<React.Fragment>
			<div className="lesson_result_container">
				<div className="lesson_result">
					<Player
						autoplay={true}
						loop={true}
						src="https://assets3.lottiefiles.com/packages/lf20_UJNc2t.json"
						style={{ height: '400px' }}
					/>
					<h2>Bạn đã hoàn thành bài học</h2>
				</div>
			</div>
		</React.Fragment>
	);
};
