import React from 'react';
import { ILessonProps } from '../../model/ILessonProps';
import { Modal, Tooltip } from 'antd';
import { LESSON_MODAL } from '../../model/ILessonState';
// import { CheckCircleOutlined } from '@ant-design/icons';

import './ModalLessonReview.scss';

const AnswerInformation = ({
	userAnswer,
	rightAnswer,
}: {
	userAnswer: string;
	rightAnswer: string;
}) => {
	return (
		<div>
			<div className="text-uppercase text-muted font-weight-bold">CÂU TRẢ LỜI CỦA BẠN:</div>
			<div
				style={{
					color: '#3c3c3c',
				}}
			>
				{userAnswer}
			</div>
			<div className="text-uppercase text-muted font-weight-bold">ĐÁP ÁN ĐÚNG:</div>
			<div
				style={{
					color: '#3c3c3c',
				}}
			>
				{rightAnswer}
			</div>
		</div>
	);
};

const ModalLessonReview: React.FC<ILessonProps> = (props) => {
	const { toggleModalLessonReview, userAnswers } = props.store.LessonPage;

	return (
		<Modal
			visible={toggleModalLessonReview}
			footer={null}
			onCancel={() => props.actions.toggleModal({ type: LESSON_MODAL.REVIEW_LESSON })}
			width={768}
			wrapClassName="modal_review"
		>
			<div className="review_header">Xem bảng điểm của bạn!</div>
			<div className="review_subHeader">Nhấp vào ô bên dưới để hiện đáp án</div>
			<div className="review_container">
				{userAnswers?.map((item, index) => (
					<Tooltip
						key={index}
						placement="topLeft"
						overlayStyle={{
							backgroundColor: 'transparent',
						}}
						overlayClassName="answer_information"
						title={
							<AnswerInformation userAnswer={item.userAnswer} rightAnswer={item.rightAnswer} />
						}
						trigger={['click']}
					>
						<div className={`review_item ${item.right ? 'right' : 'wrong'}`}>
							<div className="question">{item.question}</div>
							<div className="answer">{item.rightAnswer}</div>
						</div>
					</Tooltip>
				))}
			</div>
		</Modal>
	);
};

export default ModalLessonReview;
