import React from 'react';
import { AnswerStatus, IAnswer, IQuestion } from '../../model/ILessonState';
import classnames from 'classnames';
import volumeIcon from '../../../../assets/images/icons/volume.svg';

import './index.scss';

interface IProps {
	question: IQuestion;
	onSelectAnswer: (answer: IAnswer) => void;
	answerStatus: AnswerStatus;
}

const wordReducer = (state: IAnswer[], action: { type: 'add' | 'remove'; word: IAnswer }) => {
	switch (action.type) {
		case 'add':
			return [...state, action.word];
		case 'remove':
			const update = [...state];
			const wordIndex = state.findIndex((item) => item._id === action.word._id);
			if (wordIndex < 0) {
				return state;
			}
			update.splice(wordIndex, 1);
			return update;
		default:
			return state;
	}
};

const CorrectSentence: React.FC<IProps> = ({ question, onSelectAnswer, answerStatus }) => {
	const [selectedWord, setSelectedWord] = React.useReducer(wordReducer, []);

	React.useEffect(() => {
		playAudio(question.link);
		// eslint-disable-next-line
	}, []);

	React.useEffect(() => {
		onSelectAnswer({
			answer: selectedWord.map((word) => word.answer).join(' '),
			_id: question._id,
		});
		// eslint-disable-next-line
	}, [selectedWord]);

	const onSelectWord = (word: IAnswer) => {
		const index = selectedWord.indexOf(word);
		setSelectedWord({ word, type: index < 0 ? 'add' : 'remove' });
	};

	const renderSelectedWords = () => {
		return (
			<div className="word_list justify-content-start">
				{selectedWord.map((word) => (
					<div className="word_item" key={word._id}>
						<button className="doan_btn" onClick={() => onSelectWord(word)}>
							{word.answer}
						</button>
					</div>
				))}
			</div>
		);
	};

	const playAudio = (url: string) => {
		const audio = new Audio(`${process.env.REACT_APP_BASE_URL}/${url}`);
		audio.play();
	};

	const renderWordList = (answers: IAnswer[]) => {
		return (
			<div className="word_list">
				{answers.map((word) => (
					<div className="word_item" key={word._id}>
						<button
							className={classnames('doan_btn', {
								selected: selectedWord.indexOf(word) > -1,
								submitted: answerStatus !== 'notSubmit',
							})}
							onClick={() => {
								if (answerStatus === 'notSubmit') {
									playAudio(word.audio as string);
									onSelectWord(word);
								}
							}}
						>
							{word.answer}
						</button>
					</div>
				))}
			</div>
		);
	};

	return (
		<React.Fragment>
			<div className="challenge_content_container">
				<div className="challenge_content">
					<div className="challenge_header">
						<h1>Viết lại bằng Tiếng Anh</h1>
					</div>
					<div className="challenge_sentence">
						<div className="challenge_translate_prompt">
							<div className="d-flex align-items-center">
								<div className="example_image_wrapper">
									<div className="example_image">
										<img
											src="https://d2pur3iezf4d1j.cloudfront.net/images/52a5a774c4de18f4a4e8c91d91788347"
											alt="example_image"
										/>
									</div>
								</div>

								<div className="sentence_wrapper">
									<div className="sentence_content">
										<div dir="ltr">
											<label className="sentence_audio">
												<button onClick={() => playAudio(question.link)}>
													<span>
														<img src={volumeIcon} alt="volumeIcon" />
													</span>
												</button>
											</label>
											<span className="sentence">{question.question}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="answer_word_wrapper">
							<div className="d-block">
								<div className="answer_word">
									<div className="fill_in_answer">
										<div className="word_list_wrapper">
											<div className="word_list justify-content-start">{renderSelectedWords()}</div>
										</div>
									</div>
									<div className="word_list_wrapper">{renderWordList(question.answers)}</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default CorrectSentence;
