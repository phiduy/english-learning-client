import React from 'react';
import { AnswerStatus, IAnswer, IQuestion } from '../../model/ILessonState';
import classnames from 'classnames';

interface IProps {
	question: IQuestion;
	selectedAnswer?: IAnswer;
	onSelectAnswer: (answer: IAnswer) => void;
	answerStatus: AnswerStatus;
}

export const Quiz: React.FC<IProps> = ({
	question,
	selectedAnswer,
	onSelectAnswer,
	answerStatus,
}) => {
	return (
		<div className="challenge_content">
			<div className="challenge_header">
				<h1>{question.question}</h1>
			</div>
			<div className="challenge_options">
				{question.answers.map((item, index) => (
					<label
						className={classnames('option', {
							selected: item._id === selectedAnswer?._id,
							submitted: answerStatus !== 'notSubmit',
						})}
						key={item._id}
						onClick={() => {
							if (answerStatus === 'notSubmit') {
								onSelectAnswer(item);
							}
						}}
					>
						<div className="d-flex w-100 flex-direction-column flex-grow-1">
							<div
								className="option_image"
								style={{
									backgroundImage: `url(${item.image})`,
								}}
							/>
						</div>
						<div className="option_content">
							<span className="option_answer">{item.answer}</span>
							<span className="option_no">{index + 1}</span>
						</div>
					</label>
				))}
			</div>
		</div>
	);
};
