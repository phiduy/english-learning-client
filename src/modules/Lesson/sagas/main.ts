import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as LearnApi from '../../../api/learn';
import * as UserApi from '../../../api/user';
import { message } from 'antd';

// Handle GET Lessons
function* handleGetLessonById(action: any) {
	try {
		const res = yield call(LearnApi.getQuizById, action.payload);
		if (res.status === 200) {
			yield put(actions.getLessonByIdSuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].message, 3);
			yield put(actions.getLessonByIdFail(errors));
		}
	} catch (error) {
		yield put(actions.getLessonByIdFail(error));
	}
}

// Handle POST User History
function* handlePostUserHistory(action: any) {
	try {
		const res = yield call(UserApi.postUserHistory, action.payload);
		yield delay(200);
		if (res.status === 200) {
			yield put(actions.postUserHistorySuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.postUserHistoryFail(errors));
		}
	} catch (error) {
		yield put(actions.postUserHistoryFail(error));
	}
}

// Handle Check Point Test
function* handleCheckPointTest(action: any) {
	try {
		const res = yield call(LearnApi.checkPointTest, action.payload);
		if (res.status === 200) {
			yield put(actions.checkPointTestSuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.checkPointTestFail(errors));
		}
	} catch (error) {
		yield put(actions.checkPointTestFail(error));
	}
}

// Handle Finish Check Point Test
function* handleFinishCheckPointTest(action: any) {
	try {
		const res = yield call(LearnApi.finishCheckPointTest, action.payload);
		if (res.status === 200) {
			yield put(actions.finishCheckPointTestSuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.finishCheckPointTestFail(errors));
		}
	} catch (error) {
		yield put(actions.finishCheckPointTestFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetLessonById() {
	yield takeEvery(Keys.GET_LESSON_BY_ID, handleGetLessonById);
}
function* watchPostUserHistory() {
	yield takeEvery(Keys.POST_USER_HISTORY, handlePostUserHistory);
}
function* watchCheckPointTest() {
	yield takeEvery(Keys.CHECK_POINT_TEST, handleCheckPointTest);
}
function* watchFinishCheckPointTest() {
	yield takeEvery(Keys.FINISH_CHECK_POINT_TEST, handleFinishCheckPointTest);
}
/*-----------------------------------------------------------------*/
const sagas = [
	watchGetLessonById,
	watchPostUserHistory,
	watchCheckPointTest,
	watchFinishCheckPointTest,
];
export default sagas;
