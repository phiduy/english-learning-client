import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { ILessonState, initialState, LESSON_CLEAR, LESSON_MODAL } from './model/ILessonState';

export const name = 'LessonPage';

export const reducer: Reducer<ILessonState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.TOGGLE_MODAL:
			return onToggleModal(state, action);

		case Keys.HANDLE_USER_ANSWER:
			return onHandleUserAnswer(state, action);

		case Keys.HANDLE_CLEAR:
			return onHandleClear(state, action);

		case Keys.POST_USER_HISTORY:
			return onPostUserHistory(state, action);
		case Keys.POST_USER_HISTORY_SUCCESS:
			return onPostUserHistorySuccess(state, action);
		case Keys.POST_USER_HISTORY_FAIL:
			return onPostUserHistoryFail(state, action);

		case Keys.GET_LESSON_BY_ID:
			return onGetLessonById(state, action);
		case Keys.GET_LESSON_BY_ID_SUCCESS:
			return onGetLessonByIdSuccess(state, action);
		case Keys.GET_LESSON_BY_ID_FAIL:
			return onGetLessonByIdFail(state, action);

		case Keys.CHECK_POINT_TEST:
			return onGetCheckPointTest(state, action);
		case Keys.CHECK_POINT_TEST_SUCCESS:
			return onGetCheckPointTestSuccess(state, action);
		case Keys.CHECK_POINT_TEST_FAIL:
			return onGetCheckPointTestFail(state, action);

		case Keys.FINISH_CHECK_POINT_TEST:
			return onFinishCheckPointTest(state, action);
		case Keys.FINISH_CHECK_POINT_TEST_SUCCESS:
			return onFinishCheckPointTestSuccess(state, action);
		case Keys.FINISH_CHECK_POINT_TEST_FAIL:
			return onFinishCheckPointTestFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action
const onToggleModal = (state: ILessonState, action: IActions.IToggleModal) => {
	const { type } = action.payload;
	switch (type) {
		case LESSON_MODAL.REVIEW_LESSON:
			return {
				...state,
				toggleModalLessonReview: !state.toggleModalLessonReview,
			};
		default:
			return {
				...state,
			};
	}
};

const onHandleClear = (state: ILessonState, action: IActions.IHandleClear) => {
	const { type } = action.payload;
	switch (type) {
		case LESSON_CLEAR.ALL:
			return {
				...initialState,
			};

		default:
			return {
				...state,
			};
	}
};

const onHandleUserAnswer = (state: ILessonState, action: IActions.IHandleUserAnswer) => {
	const userAnswers = state.userAnswers;
	userAnswers?.push(action.payload);
	return {
		...state,
		userAnswers,
	};
};

const onPostUserHistory = (state: ILessonState, action: IActions.IPostUserHistory) => {
	return {
		...state,
		isCreatingUserHistoryLearn: true,
	};
};
const onPostUserHistorySuccess = (
	state: ILessonState,
	action: IActions.IPostUserHistorySuccess
) => {
	return {
		...state,
		isCreatingUserHistoryLearn: false,
		isUserHistoryCreateSuccess: true,
	};
};
const onPostUserHistoryFail = (state: ILessonState, action: IActions.IPostUserHistoryFail) => {
	return {
		...state,
		isCreatingUserHistoryLearn: false,
	};
};

const onFinishCheckPointTest = (state: ILessonState, action: IActions.IFinishCheckPointTest) => {
	return {
		...state,
		isCreatingUserHistoryLearn: true,
	};
};
const onFinishCheckPointTestSuccess = (
	state: ILessonState,
	action: IActions.IFinishCheckPointTestSuccess
) => {
	return {
		...state,
		isCreatingUserHistoryLearn: false,
	};
};
const onFinishCheckPointTestFail = (
	state: ILessonState,
	action: IActions.IFinishCheckPointTestFail
) => {
	return {
		...state,
		isCreatingUserHistoryLearn: false,
		isUserHistoryCreateSuccess: true,
	};
};

const onGetLessonById = (state: ILessonState, action: IActions.IGetLessonById) => {
	return {
		...state,
		isLoadingLessonDetail: true,
	};
};
const onGetLessonByIdSuccess = (state: ILessonState, action: IActions.IGetLessonByIdSuccess) => {
	return {
		...state,
		isLoadingLessonDetail: false,
		lessonDetail: action.payload,
	};
};
const onGetLessonByIdFail = (state: ILessonState, action: IActions.IGetLessonByIdFail) => {
	return {
		...state,
		isLoadingLessonDetail: false,
	};
};

const onGetCheckPointTest = (state: ILessonState, action: IActions.ICheckPointTest) => {
	return {
		...state,
		isLoadingLessonDetail: true,
	};
};
const onGetCheckPointTestSuccess = (
	state: ILessonState,
	action: IActions.ICheckPointTestSuccess
) => {
	return {
		...state,
		isLoadingLessonDetail: false,
		lessonDetail: action.payload,
	};
};
const onGetCheckPointTestFail = (state: ILessonState, action: IActions.ICheckPointTestFail) => {
	return {
		...state,
		isLoadingLessonDetail: false,
	};
};
