/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import { IProfileUserInfo } from './model/IProfileState';

export interface IHandleLogout extends Action {
	readonly type: Keys.HANDLE_LOGOUT;
}

//#region Update User Info IActions
export interface IUpdateUserInfo extends Action {
	readonly type: Keys.UPDATE_USER_INFO;
	payload: IProfileUserInfo;
}

export interface IUpdateUserInfoSuccess extends Action {
	readonly type: Keys.UPDATE_USER_INFO_SUCCESS;
	payload: IProfileUserInfo;
}

export interface IUpdateUserInfoFail extends Action {
	readonly type: Keys.UPDATE_USER_INFO_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Update User Info IActions
export interface IUploadImage extends Action {
	readonly type: Keys.UPLOAD_IMAGE;
	payload: Blob | File;
}

export interface IUploadImageSuccess extends Action {
	readonly type: Keys.UPLOAD_IMAGE_SUCCESS;
	payload: IProfileUserInfo;
}

export interface IUploadImageFail extends Action {
	readonly type: Keys.UPLOAD_IMAGE_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
