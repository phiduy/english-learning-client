import React from 'react';
import { Switch, Route, Redirect, useRouteMatch, RouteComponentProps } from 'react-router-dom';
import { IProfileProps } from '../model/IProfileProps';
import { Account } from './Account';
import { Password } from './Password';

interface IProps extends IProfileProps, RouteComponentProps {}

export const ProfileSubRoutes: React.FC<IProps> = (props) => {
	const { url } = useRouteMatch();
	return (
		<Switch>
			<Route path={`${url}/account`} exact={true} children={() => <Account {...props} />} />
			<Route path={`${url}/password`} exact={true} children={() => <Password {...props} />} />
			<Redirect from="*" to={`${url}/account`} />
		</Switch>
	);
};
