import React from 'react';
import { IProfileProps } from '../../model/IProfileProps';
import { AccountForm } from './Form';
interface IProps extends IProfileProps {}

export const Account: React.FC<IProps> = (props) => {
	return (
		<div className="profile_content">
			<AccountForm {...props} />
		</div>
	);
};
