import React from 'react';
import { Input, Form, Radio, DatePicker, Spin, message } from 'antd';
import { IProfileProps } from '../../../model/IProfileProps';
import { dateFormat } from '../../../../../common/constants';
import moment from 'moment';

interface IProps extends IProfileProps {}

interface IInputs {
	email: string;
	gender: string;
	name: string;
	dayOfBirth: string;
}

export const AccountForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { userInfo, isLoadingUserInfo } = props.store.MainLayout;
	const { isProcessing, isUploading } = props.store.ProfilePage;

	React.useEffect(() => {
		if (userInfo !== undefined) {
			formInstance.setFieldsValue({
				name: userInfo?.name,
				email: userInfo?.email,
				gender: userInfo?.gender,
				dayOfBirth: moment(userInfo?.dayOfBirth),
			});
		}
		// eslint-disable-next-line
	}, [userInfo]);

	const imageHandler = () => {
		const input = document.createElement('input');
		input.setAttribute('type', 'file');
		input.setAttribute('accept', 'image/*');
		input.click();
		input.onchange = () => {
			const fileList = input.files as FileList;
			const file = fileList[0];
			// file type is only image.
			if (/^image\//.test(file.type)) {
				if (file.size > 1048576 * 2) {
					message.error('Hình ảnh phải nhỏ hơn 2Mb', 2);
				} else {
					props.actions.uploadImage(file);
				}
			} else {
				console.log('fail');
			}
		};
	};

	const onFinish = (data: IInputs) => {
		props.actions.updateUserInfo({
			name: data.name,
			gender: data.gender,
			dayOfBirth: moment(data.dayOfBirth).toISOString(),
		});
	};
	return (
		<React.Fragment>
			<div className="profile_information">
				<Form
					form={formInstance}
					className="input_group_wrapper"
					layout="vertical"
					initialValues={{
						gender: 'male',
					}}
					onFinish={onFinish}
				>
					<div className="profile_title">
						<h1>Cài đặt tài khoản</h1>
						<button
							className="doan_btn doan_btn_success"
							disabled={isProcessing}
							type="submit"
							style={{ width: 'auto' }}
						>
							{isProcessing ? 'Đang xử lý...' : 'Lưu thay đổi'}
						</button>
					</div>
					<Spin spinning={isLoadingUserInfo}>
						<table className="profile_field">
							<tbody>
								<tr>
									<td className="profile_label">
										<label>Họ và tên</label>
									</td>
									<td className="profile_input">
										<Form.Item
											name="name"
											rules={[
												{
													max: 256,
												},
											]}
										>
											<Input disabled={isProcessing} size="large" />
										</Form.Item>
									</td>
								</tr>
								<tr>
									<td className="profile_label">
										<label>Email</label>
									</td>
									<td className="profile_input">
										<Form.Item name="email">
											<Input disabled={true} size="large" />
										</Form.Item>
									</td>
								</tr>
								<tr>
									<td className="profile_label">
										<label>Giới tính</label>
									</td>
									<td className="profile_input">
										<Form.Item name="gender">
											<Radio.Group disabled={isProcessing}>
												<Radio value="male">Nam</Radio>
												<Radio value="female">Nữ</Radio>
												<Radio value="other">Khác</Radio>
											</Radio.Group>
										</Form.Item>
									</td>
								</tr>
								<tr>
									<td className="profile_label">
										<label>Ngày sinh</label>
									</td>
									<td className="profile_input">
										<Form.Item name="dayOfBirth">
											<DatePicker size="large" format={dateFormat} disabled={isProcessing} />
										</Form.Item>
									</td>
								</tr>
								<tr>
									<td className="profile_label">
										<label>Ảnh hồ sơ</label>
									</td>
									<td className="profile_input">
										<button
											className="doan_btn"
											style={{ width: 'auto' }}
											onClick={() => imageHandler()}
											type="button"
										>
											{isUploading ? 'Đang xử lý...' : 'Chọn tập tin'}
										</button>
										<div className="mt-2">kích cỡ ảnh tối đa là 1 MB</div>
									</td>
								</tr>
							</tbody>
						</table>
					</Spin>
				</Form>
			</div>
		</React.Fragment>
	);
};
