import React from 'react';
import { Input, Form } from 'antd';
// import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import { IProfileProps } from '../../../model/IProfileProps';

interface IProps extends IProfileProps {}

interface IInputs {
	oldPassword: string;
	password: string;
}

export const PasswordForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.ProfilePage;

	const onFinish = (data: IInputs) => {
		const { oldPassword, password } = data;
		if (!isProcessing) {
			props.actions.updateUserInfo({
				oldPassword,
				password,
			});
		}
	};
	return (
		<React.Fragment>
			<div className="profile_information">
				<Form
					form={formInstance}
					className="input_group_wrapper"
					layout="vertical"
					onFinish={onFinish}
				>
					<div className="profile_title">
						<h1>Mật khẩu</h1>
						<button className="doan_btn doan_btn_success" type="submit" style={{ width: 'auto' }}>
							{isProcessing ? 'Đang xử lý...' : 'Lưu thay đổi'}
						</button>
					</div>
					<table className="profile_field">
						<tbody>
							<tr>
								<td className="profile_label">
									<label>Mật khẩu hiện tại</label>
								</td>
								<td className="profile_input">
									<Form.Item
										name="oldPassword"
										rules={[
											{
												required: true,
											},
											{
												max: 256,
											},
										]}
									>
										<Input.Password disabled={isProcessing} size="large" />
									</Form.Item>
								</td>
							</tr>
							<tr>
								<td className="profile_label">
									<label>Mật khẩu mới</label>
								</td>
								<td className="profile_input">
									<Form.Item
										name="password"
										rules={[
											{
												required: true,
											},
											{
												max: 256,
											},
										]}
									>
										<Input.Password disabled={isProcessing} size="large" />
									</Form.Item>
								</td>
							</tr>
						</tbody>
					</table>
				</Form>
			</div>
		</React.Fragment>
	);
};
