import React from 'react';
import { IProfileProps } from '../../model/IProfileProps';
import { PasswordForm } from './Form';
interface IProps extends IProfileProps {}

export const Password: React.FC<IProps> = (props) => {
	return (
		<div className="profile_content">
			<PasswordForm {...props} />
		</div>
	);
};
