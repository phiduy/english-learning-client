import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Avatar } from '../../../../components';
import { IProfileProps } from '../../model/IProfileProps';

interface IProps extends IProfileProps {}

export const SideBar: React.FC<IProps> = (props) => {
	const location = useLocation();
	const { userInfo } = props.store.MainLayout;

	return (
		<div className="right_side_bar">
			<div className="side_bar_inner">
				<div className="profile_header">
					<Avatar url={userInfo?.avatar as string} />
					<h2>DuyVo</h2>
				</div>
				<ul className="profile_navigation">
					<li className={location.pathname === '/profile/account' ? 'active' : ''}>
						<Link to="/profile/account">Tài khoản</Link>
					</li>
					<li className={location.pathname === '/profile/password' ? 'active' : ''}>
						<Link to="/profile/password">Mật khẩu</Link>
					</li>
					<li>
						<span onClick={() => props.actions.handleLogout()} style={{ cursor: 'pointer' }}>
							Đăng xuất
						</span>
					</li>
				</ul>
			</div>
		</div>
	);
};
