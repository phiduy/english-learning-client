export interface IUserAuthInfo {}

export interface IProfileUserInfo {
	name?: string;
	oldPassword?: string;
	password?: string;
	gender?: string;
	dayOfBirth?: string;
	avatar?: string;
}

export interface IProfileState {
	isUploading: boolean;
	isProcessing: boolean;
}

// InitialState
export const initialState: IProfileState = {
	isProcessing: false,
	isUploading: false,
};
