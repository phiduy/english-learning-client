import { getUserInfoSuccess } from './../../../layouts/MainLayout/actions';
import { call, put, takeEvery } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as UserApi from '../../../api/user';
import { message } from 'antd';

// Handle Update User Info
function* handleUpdateUserInfo(action: any) {
	try {
		const res = yield call(UserApi.updateUserInfo, action.payload);
		if (res.status === 200) {
			message.success('Cập nhật thông tin thành công', 2);
			yield put(actions.updateUserInfoSuccess(res.data.data));
			yield put(getUserInfoSuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.updateUserInfoFail(res.data.errors));
		}
	} catch (error) {
		yield put(actions.updateUserInfoFail(error));
	}
}

// Handle Update Upload Image
function* handleUploadImage(action: any) {
	try {
		const res = yield call(UserApi.uploadImage, action.payload);
		if (res.status === 200) {
			const { url } = res.data.data;
			message.info(
				'Upload hình ảnh thành công đang cập nhật thông tin vui lòng đợi trong giây lát',
				2
			);
			yield put(actions.uploadImageSuccess(res.data.data));
			yield put(
				actions.updateUserInfo({
					avatar: url,
				})
			);
		} else {
			if (res === undefined) {
				message.error('Can not upload image', 2);
			}
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.uploadImageFail(res.data.errors));
		}
	} catch (error) {
		yield put(actions.uploadImageFail(error));
	}
}

// eslint-disable-next-line
function* handleLogoutAccount() {
	localStorage.clear();
	window.location.reload();
}

/*-----------------------------------------------------------------*/
function* watchLogoutAccount() {
	yield takeEvery(Keys.HANDLE_LOGOUT, handleLogoutAccount);
}
function* watchUpdateUserInfo() {
	yield takeEvery(Keys.UPDATE_USER_INFO, handleUpdateUserInfo);
}
function* watchUploadImage() {
	yield takeEvery(Keys.UPLOAD_IMAGE, handleUploadImage);
}
/*-----------------------------------------------------------------*/
const sagas = [watchUpdateUserInfo, watchUploadImage, watchLogoutAccount];

export default sagas;
