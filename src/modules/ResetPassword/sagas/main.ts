import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';

// Handle User Reset Password
function* handleUserResetPassword(action: any) {
	try {
		const res = yield call(AuthApi.login, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Gửi mã xác nhận kí thành công', 1);
			yield put(actions.userResetPasswordSuccess(res.data.data));
		} else {
			message.error('Gửi mã xác nhận nhập thất bại', 1);
			yield put(actions.userResetPasswordFail(res));
		}
	} catch (error) {
		yield put(actions.userResetPasswordFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserResetPassword() {
	yield takeEvery(Keys.USER_RESET_PASSWORD, handleUserResetPassword);
}
/*-----------------------------------------------------------------*/
const sagas = [watchUserResetPassword];

export default sagas;
