/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';

//#region User Reset Password IActions
export interface IUserResetPassword extends Action {
	readonly type: Keys.USER_RESET_PASSWORD;
	payload: { email: string };
}

export interface IUserResetPasswordSuccess extends Action {
	readonly type: Keys.USER_RESET_PASSWORD_SUCCESS;
	payload: {
		userInfo: {
			name: string;
			role: string;
		};
	};
}

export interface IUserResetPasswordFail extends Action {
	readonly type: Keys.USER_RESET_PASSWORD_FAIL;
	payload?: {
		errors: string[];
	};
}
//#endregion
