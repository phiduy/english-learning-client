import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as AuthApi from '../../../api/auth';
import { message } from 'antd';

// Handle User Register
function* handleUserRegister(action: any) {
	try {
		const res = yield call(AuthApi.register, action.payload);
		yield delay(500);
		if (res.status === 200) {
			message.success('Register successful', 2);
			yield put(actions.userRegisterSuccess(res));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.userRegisterFail(res.data.errors));
		}
	} catch (error) {
		yield put(actions.userRegisterFail(error));
	}
}

// Handle User Login
function* handleUserLogin(action: any) {
	try {
		const res = yield call(AuthApi.login, action.payload);
		yield delay(500);
		if (res.status === 200) {
			const { accessToken, refreshToken } = res.data.data;
			localStorage.setItem('accessToken', accessToken);
			localStorage.setItem('refreshToken', refreshToken);
			yield put(actions.userLoginSuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.userLoginFail(res.data.errors));
		}
	} catch (error) {
		yield put(actions.userLoginFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchUserRegister() {
	yield takeEvery(Keys.USER_REGISTER, handleUserRegister);
}
function* watchUserLogin() {
	yield takeEvery(Keys.USER_LOGIN, handleUserLogin);
}
/*-----------------------------------------------------------------*/
const sagas = [watchUserRegister, watchUserLogin];

export default sagas;
