import { getLocalStorage } from './../../../services/localStorage';
export interface IUserAuthInfo {
	email: string;
	password: string;
}

export interface IAuthState {
	isProcessing: boolean;
	isLoading: boolean;
	accessToken?: string | null;
	refreshToken?: string | null;
}

// InitialState
export const initialState: IAuthState = {
	isLoading: false,
	isProcessing: false,
	accessToken: undefined || getLocalStorage('accessToken'),
	refreshToken: undefined || getLocalStorage('refreshToken'),
};
