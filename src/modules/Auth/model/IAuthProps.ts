import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IAuthProps {
	store: IStore;
	actions: typeof Actions;
}
