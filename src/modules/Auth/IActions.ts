/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import { IUserAuthInfo } from './model/IAuthState';

//#region User Register IActions
export interface IUserRegister extends Action {
	readonly type: Keys.USER_REGISTER;
	payload: IUserAuthInfo;
}

export interface IUserRegisterSuccess extends Action {
	readonly type: Keys.USER_REGISTER_SUCCESS;
	payload: {
		userInfo: {
			name: string;
			role: string;
		};
	};
}

export interface IUserRegisterFail extends Action {
	readonly type: Keys.USER_REGISTER_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region User Login IActions
export interface IUserLogin extends Action {
	readonly type: Keys.USER_LOGIN;
	payload: IUserAuthInfo;
}
export interface IUserLoginSuccess extends Action {
	readonly type: Keys.USER_LOGIN_SUCCESS;
	payload: {
		accessToken: string;
		refreshToken: string;
	};
}
export interface IUserLoginFail extends Action {
	readonly type: Keys.USER_LOGIN_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion
