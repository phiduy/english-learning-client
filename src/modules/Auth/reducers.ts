import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IAuthState, initialState } from './model/IAuthState';

export const name = 'AuthPage';

export const reducer: Reducer<IAuthState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.USER_REGISTER:
			return onUserRegister(state, action);
		case Keys.USER_REGISTER_SUCCESS:
			return onUserRegisterSuccess(state, action);
		case Keys.USER_REGISTER_FAIL:
			return onUserRegisterFail(state, action);

		case Keys.USER_LOGIN:
			return onUserLogin(state, action);
		case Keys.USER_LOGIN_SUCCESS:
			return onUserLoginSuccess(state, action);
		case Keys.USER_LOGIN_FAIL:
			return onUserLoginFail(state, action);
		default:
			return state;
	}
};

// IActions: the interface of current action

const onUserRegister = (state: IAuthState, action: IActions.IUserRegister) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUserRegisterSuccess = (state: IAuthState, action: IActions.IUserRegisterSuccess) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onUserRegisterFail = (state: IAuthState, action: IActions.IUserRegisterFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onUserLogin = (state: IAuthState, action: IActions.IUserLogin) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUserLoginSuccess = (state: IAuthState, action: IActions.IUserLoginSuccess) => {
	const { accessToken, refreshToken } = action.payload;
	return {
		...state,
		isProcessing: false,
		accessToken,
		refreshToken,
	};
};
const onUserLoginFail = (state: IAuthState, action: IActions.IUserLoginFail) => {
	return {
		...state,
		isProcessing: false,
	};
};
