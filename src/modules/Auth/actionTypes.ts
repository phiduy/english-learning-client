/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IUserRegister
	| IActions.IUserRegisterSuccess
	| IActions.IUserRegisterFail
	| IActions.IUserLogin
	| IActions.IUserLoginSuccess
	| IActions.IUserLoginFail;

export default ActionTypes;
