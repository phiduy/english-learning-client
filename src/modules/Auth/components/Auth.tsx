import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IAuthProps } from '../model/IAuthProps';
import { CloseOutlined } from '@ant-design/icons';
import { RegisterForm, LoginForm } from './Form';

import './index.scss';

interface IProps extends RouteComponentProps, IAuthProps {}

export const AuthPage: React.FC<IProps> = (props) => {
	const [toggleForm, setToggleForm] = React.useState(false);

	return (
		<div className="h-100">
			<div>
				<div className="auth_wrapper">
					<div className="auth_inner">
						<div className="auth_quit_btn">
							<div className="doan_btn_quit" onClick={() => props.history.push('/learn')}>
								<CloseOutlined />
							</div>
						</div>

						<div className="auth_toggle_register_btn">
							<button
								className="doan_btn"
								style={{ minWidth: 'auto' }}
								onClick={() => setToggleForm(!toggleForm)}
							>
								{toggleForm ? 'Login' : 'Sign Up'}
							</button>
						</div>

						<div className="auth_form">
							{toggleForm ? <RegisterForm {...props} /> : <LoginForm {...props} />}
							<div className="auth_form_footer" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};
