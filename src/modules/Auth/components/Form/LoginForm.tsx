import React from 'react';
import { Input, Form } from 'antd';
// import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import { IAuthProps } from '../../model/IAuthProps';

interface IProps extends IAuthProps {}

interface IInputs {
	email: string;
	password: string;
}

export const LoginForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.AuthPage;

	const onFinish = (data: IInputs) => {
		const { email, password } = data;
		if (!isProcessing) {
			props.actions.userLogin({
				email,
				password,
			});
		}
	};
	return (
		<React.Fragment>
			<div>
				<h1>Log in</h1>
				<Form
					form={formInstance}
					className="input_group_wrapper"
					layout="vertical"
					onFinish={onFinish}
				>
					<Form.Item
						name="email"
						rules={[
							{
								type: 'email',
							},
							{
								required: true,
							},
							{
								max: 256,
							},
						]}
					>
						<Input
							className="doan_form_group"
							placeholder="Email"
							disabled={isProcessing}
							size="large"
						/>
					</Form.Item>

					<Form.Item
						name="password"
						rules={[
							{
								required: true,
							},
							{
								max: 256,
							},
						]}
						hasFeedback={true}
					>
						<Input.Password
							className="doan_form_group"
							placeholder="Mật khẩu"
							disabled={isProcessing}
							size="large"
						/>
					</Form.Item>

					<button className="doan_btn doan_btn_primary" type="submit">
						{isProcessing ? 'Đang xử lý...' : 'Đăng nhập'}
					</button>
				</Form>
			</div>
		</React.Fragment>
	);
};
