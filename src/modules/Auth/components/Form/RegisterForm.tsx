import React from 'react';
import { Input, Form } from 'antd';
import { IAuthProps } from '../../model/IAuthProps';

interface IProps extends IAuthProps {}

interface IInputs {
	email: string;
	password: string;
}

export const RegisterForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.AuthPage;

	const onFinish = (data: IInputs) => {
		const { email, password } = data;
		if (!isProcessing) {
			props.actions.userRegister({
				email,
				password,
			});
		}
	};
	return (
		<React.Fragment>
			<div>
				<h1>Create your profile</h1>
				<Form
					form={formInstance}
					className="input_group_wrapper"
					layout="vertical"
					onFinish={onFinish}
				>
					<Form.Item
						name="email"
						rules={[
							{
								type: 'email',
							},
							{
								required: true,
							},
							{
								max: 256,
							},
						]}
					>
						<Input
							className="doan_form_group"
							placeholder="Email"
							disabled={isProcessing}
							size="large"
						/>
					</Form.Item>

					<Form.Item
						name="password"
						rules={[
							{
								required: true,
							},
							{
								max: 256,
							},
						]}
						hasFeedback={true}
					>
						<Input.Password
							className="doan_form_group"
							placeholder="Mật khẩu"
							disabled={isProcessing}
							size="large"
						/>
					</Form.Item>

					<Form.Item
						name="confirm"
						dependencies={['password']}
						hasFeedback={true}
						rules={[
							{
								required: true,
							},
							{
								max: 256,
							},
							({ getFieldValue }) => ({
								validator(rule, value) {
									if (!value || getFieldValue('password') === value) {
										return Promise.resolve();
									}
									return Promise.reject('Xác nhận lại mật khẩu không khớp!');
								},
							}),
						]}
					>
						<Input.Password
							className="doan_form_group"
							placeholder="Xác nhận mật khẩu"
							disabled={isProcessing}
							size="large"
						/>
					</Form.Item>

					<button className="doan_btn doan_btn_primary" type="submit">
						{isProcessing ? 'Đang xử lý' : 'Đăng ký'}
					</button>
				</Form>
			</div>
		</React.Fragment>
	);
};
