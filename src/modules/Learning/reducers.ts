import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { ILearningState, initialState } from './model/ILearningState';

export const name = 'LearningPage';

export const reducer: Reducer<ILearningState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.GET_LESSONS:
			return onGetLessons(state, action);
		case Keys.GET_LESSONS_SUCCESS:
			return onGetLessonsSuccess(state, action);
		case Keys.GET_LESSONS_FAIL:
			return onGetLessonsFail(state, action);

		case Keys.GET_USER_HISTORY:
			return onGetUserHistory(state, action);
		case Keys.GET_USER_HISTORY_SUCCESS:
			return onGetUserHistorySuccess(state, action);
		case Keys.GET_USER_HISTORY_FAIL:
			return onGetUserHistoryFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onGetLessons = (state: ILearningState, action: IActions.IGetLessons) => {
	return {
		...state,
		isLoadingLessons: true,
	};
};
const onGetLessonsSuccess = (state: ILearningState, action: IActions.IGetLessonsSuccess) => {
	return {
		...state,
		isLoadingLessons: false,
		lessonRecords: action.payload,
	};
};
const onGetLessonsFail = (state: ILearningState, action: IActions.IGetLessonsFail) => {
	return {
		...state,
		isLoadingLessons: false,
	};
};

const onGetUserHistory = (state: ILearningState, action: IActions.IGetUserHistory) => {
	return {
		...state,
		isLoadingHistory: true,
	};
};
const onGetUserHistorySuccess = (
	state: ILearningState,
	action: IActions.IGetUserHistorySuccess
) => {
	return {
		...state,
		isLoadingHistory: false,
		userHistoryRecords: action.payload,
		chartInfo: action.payload.pop(),
	};
};
const onGetUserHistoryFail = (state: ILearningState, action: IActions.IGetUserHistoryFail) => {
	return {
		...state,
		isLoadingHistory: false,
	};
};
