import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { ILearningProps } from '../../model/ILearningProps';
import { SkillNode } from '../SkillNode';
import { Spin } from 'antd';
import checkPoint from '../../../../assets/images/icons/checkpoint-castle-unlocked.svg';
import classnames from 'classnames';

interface IProps extends ILearningProps, RouteComponentProps {}

const CheckPointNode: React.FC<{ level: string; props: IProps }> = ({ level, props }) => {
	return (
		<div
			className="skill_wrapper checkpoint"
			style={{
				transform: 'translateY(60%)',
				backgroundColor: 'white',
				border: '2px solid #e5e5e5',
				borderRadius: '50%',
				zIndex: 1,
			}}
		>
			<div className="skill_inner">
				<div className="skill_content">
					<div className="skill_icon_wrapper">
						<div className="skill_icon_inner">
							<div className="skill_icon_content">
								<div className="skill_icon_border">
									<div className="w-100 h-100 mx-auto" />
								</div>
								<div
									className="skill_icon"
									onClick={() => props.history.push(`/lesson/${level}?level=${level}`)}
								>
									<div className="icon">
										<img src={checkPoint} style={{ width: '50%' }} alt="icon" />
									</div>
									<span
										className="w-100 text-center"
										style={{
											position: 'absolute',
											color: '#3d3d3d',
											left: 0,
											top: '34%',
											fontWeight: 700,
										}}
									>
										{level}
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export const SkillTree: React.FC<IProps> = (props) => {
	const { lessonRecords, isLoadingLessons } = props.store.LearningPage;
	const { userInfo } = props.store.MainLayout;

	const enableLevel = (level: number) => {
		return (userInfo?.levelInfo.level as number) >= level;
	};

	const renderNodeTree = () => {
		return Object.keys(lessonRecords).map((level) => {
			if (Object.keys(lessonRecords[level]).length < 1) {
				return null;
			}
			return (
				<div
					className={classnames('skill_tree_wrapper', {
						disabled: !enableLevel(parseInt(level, 10)),
						paddingTop: parseInt(level, 10) > 1,
					})}
					key={level}
				>
					<div className="skill_tree">
						{Object.keys(lessonRecords[level]).map((order) => (
							<div className="skill_grid_container" key={order}>
								{lessonRecords[level][order].map((skillNode) => (
									<SkillNode
										key={skillNode._id}
										props={props}
										disabled={!enableLevel(parseInt(level, 10))}
										item={skillNode}
									/>
								))}
							</div>
						))}
						<CheckPointNode props={props} level={level} />
					</div>
				</div>
			);
		});
	};

	return (
		<div className="skill_tree_container">
			<div className="skill_tree_wrapper mt-0">
				<div className="skill_tree">
					<Spin spinning={isLoadingLessons}>{renderNodeTree()}</Spin>
				</div>
			</div>
		</div>
	);
};
