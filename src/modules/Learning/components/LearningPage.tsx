import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { HeaderBar } from '../../../components';
import { ILearningProps } from '../model/ILearningProps';
import { RightSide } from './RightSide';

import './index.scss';
import { SkillTree } from './SkillTree';

interface IProps extends RouteComponentProps, ILearningProps {}

export const LearningPage: React.FC<IProps> = (props) => {
	React.useEffect(() => {
		props.actions.getLessons({
			page: 0,
			limit: 10,
			maxLevel: 3,
		});
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<HeaderBar />
			<div className="lesson_container">
				<RightSide {...props} />
				<div className="lesson_content_wrapper">
					<div className="lesson_content">
						<SkillTree {...props} />
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
