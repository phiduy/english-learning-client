import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { ILearningProps } from '../../model/ILearningProps';
import { Line } from 'react-chartjs-2';
import { handleLogout } from '../../../Profile/actions';
import { useDispatch } from 'react-redux';

interface IProps extends RouteComponentProps, ILearningProps {}

const LineChart: React.FC<{
	labels: string[];
	chartData: { vocabularies: number[]; lessons: number[] };
}> = ({ labels, chartData }) => {
	const data = {
		labels,
		datasets: [
			{
				label: 'Từ vựng',
				data: chartData.vocabularies,
				fill: false,
				backgroundColor: 'rgb(255, 200, 0)',
				borderColor: 'rgba(255, 198, 0,0.5)',
			},
			{
				label: 'Bài học',
				data: chartData.lessons,
				fill: false,
				backgroundColor: '#1cb0f6',
				borderColor: '#ddf4ff',
			},
		],
	};

	const options = {
		scales: {
			yAxes: [
				{
					ticks: {
						beginAtZero: true,
					},
				},
			],
		},
	};
	return <Line data={data} options={options} />;
};

export const RightSide: React.FC<IProps> = (props) => {
	const { userHistoryRecords } = props.store.LearningPage;
	const dispatch = useDispatch();

	const weekdays = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];

	const getDayName = (date: string) => {
		return weekdays[new Date(date).getDay()];
	};

	const labels = [];
	const vocabularies = [];
	const lessons = [];

	for (const record of userHistoryRecords) {
		labels.push(getDayName(record.date));
		vocabularies.push(record.totalVocabularyPackages);
		lessons.push(record.totalGameLevel);
	}

	React.useEffect(() => {
		const date = new Date();
		date.setDate(date.getDate() - 7);

		props.actions.getUserHistory({
			day: '7',
			beginDate: date.getTime().toString(),
		});
		// eslint-disable-next-line
	}, []);

	return (
		<div className="right_side_content">
			<div>
				<div className="medal_container">
					<div className="medal_top">
						<div className="medal_title">
							<h2 className="m-0">Tổng kết tuần</h2>
						</div>
					</div>
					<hr />
					<div className="leader_board">
						<LineChart labels={labels} chartData={{ vocabularies, lessons }} />
					</div>
				</div>
			</div>
			<div className="invite_user_login">
				<h2>Thông tin tài khoản</h2>
				<button
					className="doan_btn doan_btn_success mb-2"
					onClick={() => props.history.push('/profile')}
				>
					Tài khoản
				</button>
				<button
					className="doan_btn doan_btn_primary"
					onClick={() => {
						dispatch(handleLogout());
					}}
				>
					Đăng xuất
				</button>
			</div>
		</div>
	);
};
