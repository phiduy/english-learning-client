import { ILesson } from '../../Lesson/model/ILessonState';

export interface ILessonRecord {
	[x: string]: {
		[x: string]: ILesson[];
	};
}

export interface IUserHistory {
	date: string;
	isLogin: true;
	totalGameLevel: number;
	loginDay?: number;
	totalVocabularyPackages: number;
	userId: string;
	_id: string;
}
export interface ILearningState {
	lessonRecords: ILessonRecord;
	userHistoryRecords: IUserHistory[];
	charInfo: {
		loginDay: number;
		totalGameLevel: number;
		totalVocabularyPackages: number;
	};

	pagination: {
		totalPage: number;
		totalRecord: number;
	};
	isProcessing: boolean;
	isLoadingHistory: boolean;
	isLoadingLessons: boolean;
}

// InitialState
export const initialState: ILearningState = {
	lessonRecords: {},
	userHistoryRecords: [],
	charInfo: {
		loginDay: 0,
		totalGameLevel: 0,
		totalVocabularyPackages: 0,
	},
	pagination: {
		totalPage: 0,
		totalRecord: 0,
	},
	isLoadingHistory: false,
	isLoadingLessons: false,
	isProcessing: false,
};
