/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IGetUserHistory
	| IActions.IGetUserHistorySuccess
	| IActions.IGetUserHistoryFail
	| IActions.IGetLessons
	| IActions.IGetLessonsSuccess
	| IActions.IGetLessonsFail;

export default ActionTypes;
