import { request } from '../config/axios';
import { IProfileUserInfo } from '../modules/Profile/model/IProfileState';

export const getUserInfo = () => {
	const endpoint = '/users';
	return request(endpoint, 'GET', null);
};

export const updateUserInfo = (data: IProfileUserInfo) => {
	const endpoint = '/users';
	return request(endpoint, 'PUT', data);
};

export const getUserHistory = (data: { day: string; beginDate: string }) => {
	const endpoint = `/usersHistory?day=${data.day}&&beginDate=${data.beginDate}`;
	return request(endpoint, 'GET', null);
};

export const postUserHistory = (data: {
	isAddGamesLevel: boolean;
	isAddVocabularyLevel: boolean;
	gameId: string;
}) => {
	const endpoint = '/usersHistory';
	return request(endpoint, 'POST', data);
};

export const uploadImage = (data: File | Blob) => {
	const endpoint = '/uploadFiles/images';
	const formData = new FormData();
	formData.append('file', data);

	return request(endpoint, 'POST', formData);
};
