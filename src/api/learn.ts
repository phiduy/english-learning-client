import { request } from '../config/axios';

export const getListQuiz = (data: { page: number; limit: number; maxLevel: number }) => {
	const endpoint = `/games?page=${data.page}&limit=${data.limit}&maxLevel=${data.maxLevel}`;
	return request(endpoint, 'GET', null);
};
export const getQuizById = (data: { _id: string }) => {
	const endpoint = `/games/${data._id}`;
	return request(endpoint, 'GET', null);
};

export const checkPointTest = (data: { level: string }) => {
	const endpoint = `/gameTests?level=${data.level}`;
	return request(endpoint, 'GET', null);
};

export const finishCheckPointTest = (data: { _id: string }) => {
	const endpoint = `/gameTests/finish/${data._id}`;
	return request(endpoint, 'PUT', null);
};
