import { request } from '../config/axios';
import { IUserAuthInfo } from '../modules/Auth';

export const login = (data: any) => {
	const endpoint = '/users/login';
	return request(endpoint, 'POST', data);
};

export const register = (data: IUserAuthInfo) => {
	const endpoint = '/users/register';
	return request(endpoint, 'POST', data);
};
