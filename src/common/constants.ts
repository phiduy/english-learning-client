import { IError, IValidateMessage } from './interfaces';
export const dateFormat = 'DD/MM/YYYY';

export const SSL = 'https://';

export const soundEffect = {
	right: `${process.env.PUBLIC_URL}/sound/right-answer.mp3`,
	wrong: `${process.env.PUBLIC_URL}/sound/wrong-answer.mp3`,
	steak: `${process.env.PUBLIC_URL}/sound/steak.mp3`,
};

export const errorLessonMessages: IError[] = [
	{
		code: 'usersHistoryGameIdExisted',
		error: 'Lịch sử đã được thêm',
	},
];

export const errorProfileMessages: IError[] = [
	{
		code: 'invalid',
		error: 'Lịch sử đã được thêm',
	},
];

// ===//==============================//===//
// ${label} or ${name} - Get the name or label of current input
// When you custom label of FormItem, the value of label will be 'undefined'
// so you must add message property to the rule
// ===//==============================//===//
// === Descriptions about Antd ValidateMessages ===//
// === Link: https://github.com/react-component/field-form/blob/master/src/utils/messages.ts
/* eslint-disable */
export const validateMessages: IValidateMessage = {
	required: "'${label}' không được bỏ trống!",
	whitespace: "'${label}' không được bỏ trống!",
	string: {
		len: "'${label}' phải có đúng ${len} kí tự!",
		min: "'${label}' phải có tối thiểu ${min} kí tự!",
		max: "'${label}' không thể dài hơn ${max} kí tự!",
		range: "'${label}' phải trong khoản từ ${min} đến ${max} kí tự!",
	},
	date: {
		format: "'${label}' định dạng thời gian không hợp lệ!",
		parse: "'${label}' không thể chuyển sang định dạng ngày!",
		invalid: "'${label}' thời gian không hợp lệ!",
	},
	number: {
		len: "'${label}' phải bằng ${len}!",
		min: "'${label}' không thể nhỏ hơn ${min}!",
		max: "'${label}' khổng thể lớn hơn ${max}!",
		range: "'${label}' phải ở khoản từ ${min} đến ${max}!",
	},
	array: {
		len: "'${name}' phải có đúng ${len} về độ dài của mảng!",
		min: "'${name}' không được bé hơn ${min} về độ dài của mảng!",
		max: "'${name}' không được lớn hơn ${max} về độ dài của mảng!",
		range: "'${name}' phải trong khoản từ ${min} đến ${max} về độ dài của mảng!",
	},
};
