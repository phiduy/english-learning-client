import * as React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import IStore from './redux/store/IStore';
import { LoadingScreen } from './components';
import { IAuthState } from './modules/Auth';
import { AuthLayout } from './layouts/index';
import { mainRoutes, authRoutes } from './routes';
import MainLayout from './layouts/MainLayout/components/MainLayoutContainer';
interface IProps {
	AuthPage: IAuthState;
	router: any;
}
const App: React.FC<IProps> = (props) => {
	const { accessToken } = props.AuthPage;
	return (
		<React.Fragment>
			<ConfigProvider>
				<Router>
					<React.Suspense fallback={<LoadingScreen size="large" />}>
						{accessToken ? <MainLayout routes={mainRoutes} /> : <AuthLayout routes={authRoutes} />}
					</React.Suspense>
				</Router>
			</ConfigProvider>
		</React.Fragment>
	);
};

const mapStateToProps = (store: IStore) => ({
	AuthPage: store.AuthPage,
	router: store.router,
});
export default connect(mapStateToProps)(App);
